from pong_files.Game import Game
from pong_files.utils import WIDTH, HEIGHT

if __name__ == "__main__":
    game = Game(WIDTH, HEIGHT)
    game.run()
