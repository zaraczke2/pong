import pygame
from pong_files.utils import HEIGHT, WIDTH, WHITE

class Paddle:
    COLOR = WHITE
    SPEED = 7

    def __init__(self, x, y, width, height) -> None:
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.move = 0
        self.initial_y = y

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if self.x < WIDTH // 2:  # Left paddle
                if event.key == pygame.K_w:
                    self.move = -self.SPEED
                if event.key == pygame.K_s:
                    self.move = self.SPEED
            else:  # Right paddle
                if event.key == pygame.K_UP:
                    self.move = -self.SPEED
                if event.key == pygame.K_DOWN:
                    self.move = self.SPEED
        if event.type == pygame.KEYUP:
            if event.key in (pygame.K_w, pygame.K_s, pygame.K_UP, pygame.K_DOWN):
                self.move = 0

    def update(self):
        self.y += self.move
        self.y = max(self.y, 0)
        self.y = min(self.y, HEIGHT - self.height)

    def draw(self, win):
        pygame.draw.rect(win, self.COLOR, (self.x, self.y, self.width, self.height))

    def reset(self):
        self.y = self.initial_y  # Reset position to initial
