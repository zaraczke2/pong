import pygame
import sys
from pong_files.Paddle import Paddle
from pong_files.Ball import Ball
from pong_files.utils import WIDTH, HEIGHT, BLACK, WHITE, HIT_PADDLE_SOUND, SCORE_SOUND

class Game:
    def __init__(self, width, height):
        pygame.init()
        pygame.mixer.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('Pong Game')
        self.clock = pygame.time.Clock()
        self.running = True

        # Initialize paddles
        self.left_paddle = Paddle(10, height // 2 - 70, 20, 140)
        self.right_paddle = Paddle(width - 30, height // 2 - 70, 20, 140)

        # Initialize ball
        self.ball = Ball(width // 2, height // 2)

        # Initialize scores
        self.left_score = 0
        self.right_score = 0
        self.font = pygame.font.SysFont('Arial', 30)

        # Load sounds
        self.hit_paddle_sound = pygame.mixer.Sound(HIT_PADDLE_SOUND)
        self.score_sound = pygame.mixer.Sound(SCORE_SOUND)

        # Define button rectangles
        self.start_button_rect = pygame.Rect(self.width // 2 - 60, 20, 120, 40)
        self.reset_button_rect = pygame.Rect(self.width // 2 - 60, 70, 120, 40)

        # Game state
        self.game_active = False

    def check_collision(self):
        if self.ball.x - self.ball.RADIUS <= self.left_paddle.x + self.left_paddle.width:
            if self.left_paddle.y < self.ball.y < self.left_paddle.y + self.left_paddle.height:
                self.ball.speed_x = -self.ball.speed_x
                offset = (self.left_paddle.y + self.left_paddle.height / 2) - self.ball.y
                self.ball.speed_y += offset / 20
                self.hit_paddle_sound.play()
                self.ball.change_color()
                self.ball.increase_speed()

        if self.ball.x + self.ball.RADIUS >= self.right_paddle.x:
            if self.right_paddle.y < self.ball.y < self.right_paddle.y + self.right_paddle.height:
                self.ball.speed_x = -self.ball.speed_x
                offset = (self.right_paddle.y + self.right_paddle.height / 2) - self.ball.y
                self.ball.speed_y += offset / 20
                self.hit_paddle_sound.play()
                self.ball.change_color()
                self.ball.increase_speed()

    def update_score(self):
        if self.ball.x - self.ball.RADIUS <= 0:
            self.right_score += 1
            self.score_sound.play()
            self.ball.reset()
            self.left_paddle.reset()
            self.right_paddle.reset()
            self.game_active = False
        if self.ball.x + self.ball.RADIUS >= self.width:
            self.left_score += 1
            self.score_sound.play()
            self.ball.reset()
            self.left_paddle.reset()
            self.right_paddle.reset()
            self.game_active = False

    def draw_scores(self):
        left_score_text = self.font.render(f"{self.left_score}", True, WHITE)
        right_score_text = self.font.render(f"{self.right_score}", True, WHITE)
        self.screen.blit(left_score_text, (self.width // 4, 20))
        self.screen.blit(right_score_text, (self.width * 3 // 4, 20))

    def draw_buttons(self):
        # Start button
        pygame.draw.rect(self.screen, BLACK, self.start_button_rect)
        pygame.draw.rect(self.screen, WHITE, self.start_button_rect, 2)
        start_button_text = self.font.render('Start', True, WHITE)
        start_text_rect = start_button_text.get_rect(center=self.start_button_rect.center)
        self.screen.blit(start_button_text, start_text_rect)
        # Reset button
        pygame.draw.rect(self.screen, BLACK, self.reset_button_rect)
        pygame.draw.rect(self.screen, WHITE, self.reset_button_rect, 2)
        reset_button_text = self.font.render('Reset', True, WHITE)
        reset_text_rect = reset_button_text.get_rect(center=self.reset_button_rect.center)
        self.screen.blit(reset_button_text, reset_text_rect)

    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    # Check if Start button is clicked
                    if self.start_button_rect.collidepoint(mouse_x, mouse_y):
                        self.game_active = True
                        self.ball.reset()
                        self.left_paddle.reset()
                        self.right_paddle.reset()
                    # Check if Reset button is clicked
                    if self.reset_button_rect.collidepoint(mouse_x, mouse_y):
                        self.reset_game()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        self.game_active = True
                        self.ball.reset()
                        self.left_paddle.reset()
                        self.right_paddle.reset()

                self.left_paddle.handle_event(event)
                self.right_paddle.handle_event(event)

            if self.game_active:
                self.left_paddle.update()
                self.right_paddle.update()
                self.ball.update()
                self.check_collision()
                self.update_score()

            self.screen.fill(BLACK)
            self.left_paddle.draw(self.screen)
            self.right_paddle.draw(self.screen)
            self.ball.draw(self.screen)
            self.draw_scores()
            self.draw_buttons()

            pygame.display.flip()
            self.clock.tick(60)

        pygame.quit()
        sys.exit()

    def reset_game(self):
        self.left_score = 0
        self.right_score = 0
        self.ball.reset()
        self.left_paddle.reset()
        self.right_paddle.reset()
        self.game_active = False
