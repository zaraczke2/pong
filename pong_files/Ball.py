import pygame
import random
import math
from pong_files.utils import WHITE, WIDTH, HEIGHT, HIT_WALL_SOUND

class Ball:
    COLORS = [WHITE, (255, 0, 0), (0, 255, 0), (0, 0, 255)]
    RADIUS = 10
    SPEED = 7

    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.hit_wall_sound = pygame.mixer.Sound(HIT_WALL_SOUND)
        self.color = WHITE

    def update(self):
        self.x += self.speed_x
        self.y += self.speed_y

        if self.y - self.RADIUS <= 0 or self.y + self.RADIUS >= HEIGHT:
            self.speed_y = -self.speed_y
            self.hit_wall_sound.play()

    def draw(self, win):
        pygame.draw.circle(win, self.color, (self.x, self.y), self.RADIUS)

    def reset(self):
        self.x = WIDTH // 2
        self.y = HEIGHT // 2

        angle = random.uniform(0, 2 * math.pi)
        self.speed_x = self.SPEED * math.cos(angle)
        self.speed_y = self.SPEED * math.sin(angle)
        self.color = WHITE

    def change_color(self):
        new_color = self.color
        while new_color == self.color:
            new_color = random.choice(self.COLORS)
        self.color = new_color

    def increase_speed(self):
        self.speed_x *= 1.1
        self.speed_y *= 1.1
