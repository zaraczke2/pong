WIDTH, HEIGHT = 800, 600
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
WIDTH_PADDLE, HEIGHT_PADDLE = 20, 100
HIT_PADDLE_SOUND = 'sounds/pilka_paletka.mp3'
HIT_WALL_SOUND = 'sounds/pilka_sciana.mp3'
SCORE_SOUND = 'sounds/punkt.mp3'
